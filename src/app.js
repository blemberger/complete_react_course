console.log('App.js is running!');

// JSX - JavaScript XML

const app = {
  title: 'Indecision App',
  subtitle: 'Decisions...',
  options: []
};

const formSubmitted = (e) => {
  e.preventDefault();
  const option = e.target.elements.option.value;
  if (option) {
      app.options.push(option);
      e.target.elements.option.value=''
  }
  renderApp();
};

const removeOptions = (e) => {
  app.options = [];
  renderApp();
};

const onMakeDecision = (e) => {
  const randomNum = Math.floor(Math.random() * app.options.length);
  const option = app.options[randomNum];
  alert(option);
};

const appRoot = document.getElementById('app');

const renderApp = () => {
  const template = (
    <div>
      <h1>{app.title}</h1>
      {app.subtitle && <p>{app.subtitle}</p>}
      <p>{(app.options && (app.options.length > 0)) ? 'Here are your options' : 'No options'}</p>
      <button disabled={app.options.length === 0} onClick={onMakeDecision}>What should I do?</button>
      <button onClick={removeOptions}>Remove All</button>
      <ol>
        {/* Loop over app.options array and generate a <li> tag for each one */}
        {
          app.options.map((option) => (<li key={option}>{option}</li>))
        }
      </ol>
      <form onSubmit={formSubmitted}>
        <input type="text" name="option"/>
        <button>Save</button>
      </form>
    </div>
  );
  ReactDOM.render(template, appRoot);
}

renderApp();
