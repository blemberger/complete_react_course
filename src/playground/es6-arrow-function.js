function square(x) {
  return x * x;
}

console.log(square(8));

// const squareArrow = (x) => {
//   return x * x;
// }

// Shorthand notation for arrow functions
const squareArrow = (x) => x * x;

console.log(squareArrow(9));

const getFirstName = (fullName) => {
  return fullName.split(' ')[0];
}

console.log(getFirstName('Brian Lemberger'));

const getFirstNameConcise = (fullName) => fullName.split(' ')[0];

console.log(getFirstNameConcise('Mary Jo Zervas'));
