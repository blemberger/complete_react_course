// 'arguments' no longer bound in arrow functions

const add = function(a, b) { // ES 5
  console.log(arguments);
  return a + b;
}

console.log(add(55, 1, 100)); // 56

const addArrow = (a, b) => { // ES 6
  // console.log(arguments);
  return a + b;
}

console.log(addArrow(55, 2, 100)); // 57

const addArrow2 = (a, b) => a + b; // expression syntax

console.log(addArrow2(55, 3, 100)); // 58

// this is no longer bound in arrow functions
// In ES5, it was previously bound to the object containing
// the function definition

const user = {
  name: 'Brian',
  cities: ['Buffalo Grove', 'Arlington Hts.', 'Mount Prospect'],
  printWhereLived: function() {
    const that = this; // ES5 common work-around, this is bound to user object
    this.cities.forEach(function(city) {
      console.log(that.name + ' has lived in ' + city); // this is not bound in this scope
    });
  }
}

user.printWhereLived();

const user2 = {
  name: 'Brian',
  cities: ['Buffalo Grove', 'Arlington Hts.', 'Mount Prospect'],
  getMessagesWhereLived() { // method notation new to ES6
    return this.cities.map((city) =>
    /* this *is* bound to containing object in this scope, because arrow functions
        do not bind 'this' */
      this.name + ' has lived in ' + city
    );
  }
}

console.log(user2.getMessagesWhereLived());

// Challenge

const multiplier = {
  numbers: [1, -4, 8, 3, -5],
  multiplyBy: 5,
  multiply() {
    return this.numbers.map((number) => number * this.multiplyBy);
  }
}

console.log(multiplier.multiply());
