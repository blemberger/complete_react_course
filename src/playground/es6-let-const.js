/* var variables can be both reassigned and redefined */
var nameVar = 'Brian';
console.log('nameVar', nameVar);
var nameVar = 'Judy'; // Re-defined
console.log('nameVar', nameVar);
nameVar = 'Mary Jo';
console.log('nameVar', nameVar);

/* var variables can also be "hoisted" */
console.log('hoisted x', x);
var x = 'I am x';
console.log('x', x);

/* var variables accessed but not declared anywhere in the script are errors */
// console.log('y', y);

/* let variables allow reassignment but not redefinition */
let nameLet = 'Brian';
// let nameLet = 'Mary Jo'; // Not allowed
nameLet = 'Mary Jo'; // allowed
console.log('nameLet', nameLet);

/* let variables can also be hoisted, but only with babel, pure ES6 will disallow */
console.log('xLet hoisted', xLet);
let xLet = 'something';
/* let variables cannot be hoisted */
console.log('xLet', xLet);

/* let variables accessed but not declared anywhere are also errors */
// console.log('yLet', yLet);

/* const variables forbid reassignment and redefinition */
const nameConst = 'Brian';
//nameConst = 'Mary Jo';  // not allowed
console.log('nameConst', nameConst);

/* var, let, and const are all function scoped -
    i.e. they are not available outside of the funciton in which
    they were defined */
function getPetName() {
  const petName = 'Hal';
  return petName;
}

var petName = 'Max';
console.log('petName', petName);
console.log('getPetName', getPetName());

/* var is only function scoped, while let and const are block scopded */
var fullName = 'Brian Lemberger';

if (fullName) {
  var firstName = fullName.split(' ')[0];
}

console.log('firstName', firstName); // works, even though firstname is
                                      //  defined in if block
const fullName1 = 'Brian Lemberger';

if (fullName1) {
  let firstName1 = fullName1.split(' ')[0];
}

// console.log('firstName1', firstName1); // won't work firstName1 not in scope

const fullName2 = 'Brian Lemberger';
let firstName2 = 'Sam';
if (fullName2) {
  firstName2 = fullName2.split(' ')[0];
}

console.log('firstName2', firstName2);
